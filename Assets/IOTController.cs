﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IOTController : MonoBehaviour
{
    [SerializeField]
    private Button sendButton;

    [SerializeField]
    private Slider heartSlider, temperatureSlider;

    [SerializeField]
    private Text heartCountLabel, tempCountLabel;

    private int heartrate;
    private float temperature;

    // Start is called before the first frame update
    void Start()
    {
        this.temperature = this.temperatureSlider.value;
        this.heartrate = (int)this.heartSlider.value;
        ApiService.Login(new LoginModel { Username = "Margo@gmail.com", Password = "testtest" });
        this.temperatureSlider.onValueChanged.AddListener(x =>
        {
            temperature = x;
            tempCountLabel.text = x.ToString();
        });
        this.heartSlider.onValueChanged.AddListener(x =>
        {
            heartrate = (int)x;
            heartCountLabel.text = x.ToString();
        });
        this.sendButton.onClick.AddListener(Send);
    }

    // Update is called once per frame
    void Send()
    {
        ApiService.GetStartedProcedure("1").Then(procedure =>
        {
            procedure.Temperature = temperature;
            procedure.Heartrate = heartrate;

            ApiService.sendHeartrate(procedure);
            ApiService.sendTemperature(procedure);
        });
    }


}
