﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class StringsSet
{
    public string LoginButtonText { get; set; }

    public string SigninButtonText { get; set; }

    public string Login { get; set; }

    public string Password { get; set; }

    public string Confirm { get; set; }

    public string Firstname { get; set; }

    public string Secondname { get; set; }

    public string ChoseMode { get; set; }

    public string ProceduresManagingButton { get; set; }

    public string PatientModeButton { get; set; }

    public string StartButton { get; set; }

    public string OpenButton { get; set; }

    public string Temperature { get; set; }

    public string Heartrate { get; set; }

    public static StringsSet EnglishSet => new StringsSet
    {
        Login = "Login",
        Password = "Password",
        Confirm = "Confirm",
        Firstname = "Firstname",
        Secondname = "Secondname",
        LoginButtonText = "Login",
        SigninButtonText = "Signin",
        ChoseMode = "Choose mode",
        ProceduresManagingButton = "Procedures managing",
        PatientModeButton = "Patient mode",
        StartButton = "Start",
        OpenButton = "Open",
        Temperature = "Temperature",
        Heartrate = "Heartrate"
    };

    public static StringsSet UkrainianSet => new StringsSet
    {
        Login = "Логін",
        Password = "Пароль",
        Confirm = "Повторити пароль",
        Firstname = "Ім'я",
        Secondname = "Фамілія",
        LoginButtonText = "Вхід",
        SigninButtonText = "Реєстрація",
        ChoseMode = "Обрати режим",
        ProceduresManagingButton = "Управління процедурами",
        PatientModeButton = "Режим паціенту",
        StartButton = "Почати",
        OpenButton = "Відкрити",
        Temperature = "Температура",
        Heartrate = "Пульс"
    };
}
