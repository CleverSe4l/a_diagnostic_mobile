﻿using RSG;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UniRx;

public static class ApiService
{
    public static IPromise Login(LoginModel model)
    {
        return RequestManager.Auth(UrlPathes.Login, model);
    }

    public static IPromise Signin(SigninModel model)
    {
        return RequestManager.Auth(UrlPathes.Signin, model);
    }

    public static IPromise<List<Patient>> GetPatientsList()
    {
        return RequestManager.Get<List<Patient>>(UrlPathes.Patients);
    }

    public static IPromise<int> StartProcedure(StartingProcedure procedure)
    {
        return RequestManager.Post<int>(UrlPathes.StartProcedure, procedure);
    }

    public static IPromise StopProcedure(int patientId)
    {
        return RequestManager.Get(UrlPathes.StopProcedure + patientId);
    }

    public static IPromise postProceduresScreen(string procedureId, byte[] image)
    {
        return RequestManager.PostTexture(UrlPathes.ProcedureScreenshot, procedureId, image);
    }

    public static IPromise sendHeartrate(Procedure procedure)
    {
        Debug.Log("heart");
        return RequestManager.Post(UrlPathes.ProcedureHeartrate, procedure);
    }

    public static IPromise sendTemperature(Procedure procedure)
    {
        Debug.Log("temp");
        return RequestManager.Post(UrlPathes.ProcedureTemperature, procedure);
    }


    public static IPromise<Procedure> GetStartedProcedure(string userId)
    {
        var promise = new Promise<Procedure>();
        RequestManager.Get<Procedure>(UrlPathes.StartedProcedure + userId).Then( procedure =>
        {
            if (procedure != null) promise.Resolve(procedure);
            else Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(x => GetStartedProcedure(userId).Then(nextProcedure => promise.Resolve(nextProcedure)));
        }).Catch(ex => promise.Reject(ex));

        return promise;
    }

    public static IPromise<byte[]> GetProcedureScreen(string procedureId)
    {
        return RequestManager.GetTexture(UrlPathes.ProcedureScreenshot + procedureId);
    }
}
