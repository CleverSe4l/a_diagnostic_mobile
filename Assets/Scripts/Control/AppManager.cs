﻿using System.Collections;
using System.Collections.Generic;
using RSG.Promises;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AppManager : MonoBehaviour
{
    [SerializeField]
    private AuthView authView;

    [SerializeField]
    private ModeChoosingView modeChoosingView;

    [SerializeField]
    private PatientsListView patientsListView;

    [SerializeField]
    private CubesView cubesView;

    [SerializeField]
    private ProcedureResultView resultView;

    [SerializeField]
    private GameObject loadscreen;

    private Stack<GameObject> openedScreens = new Stack<GameObject>();

    private void Start()
    {
        this.openedScreens.Push(authView.gameObject);
        this.authView.Setup(this.Login, this.SignIn);
        this.modeChoosingView.Setup(
            () => this.SetMode(this.StartProcedure, this.StopProcedure, this.OpenProcedureResult), 
            () => this.SetMode(this.OpenProcedure));
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            this.BackToPreviousScreen();
        }
    }

    private void Login(string login, string password)
    {
        var model = new LoginModel { Username = login, Password = password };
        if (this.ValidateLoginModel(model))
        {
            this.authView.StopShowingMessage();
            this.ShowLoadingScreen(true);
            ApiService.Login(model)
                .Then(() => this.OpenNewScreen(this.modeChoosingView.gameObject))
                .Catch(ex => this.authView.ShowMessage(ex.ToString()))
                .Finally(() => this.ShowLoadingScreen(false));
        }
    }

    private void SignIn(string login, string password, string confirm)
    {
        var model = new SigninModel
        {
            Username = login,
            Password = password
        };

        if (this.ValidateSigninModel(model, confirm))
        {
            this.authView.StopShowingMessage();
            ApiService.Login(model)
                .Then(() => this.OpenNewScreen(this.modeChoosingView.gameObject))
                .Catch(ex => this.authView.ShowMessage(ex.Message))
                .Finally(() => this.ShowLoadingScreen(false));
        }
    }

    private void SetMode(UnityAction<Patient> startPatient, UnityAction<Patient> stopPatient = null, UnityAction<Patient> openPatient = null)
    {
        this.loadscreen.gameObject.SetActive(true);
        ApiService.GetPatientsList().Then(patients => 
        {
            this.patientsListView.Setup(patients, startPatient, stopPatient, openPatient);
            this.OpenNewScreen(this.patientsListView.gameObject);
            this.loadscreen.gameObject.SetActive(false);
        });
    }

    private void StartProcedure(Patient patient)
    {
        this.loadscreen.gameObject.SetActive(true);
        var startingProcedure = new StartingProcedure
        {
            PatientId = patient.Id,
            Comments = "Build a castle"
        };
        ApiService.StartProcedure(startingProcedure).Then(x =>
        {
            this.loadscreen.gameObject.SetActive(false);
        });
    }

    private void StopProcedure(Patient patient)
    {
        this.loadscreen.gameObject.SetActive(true);
        ApiService.StopProcedure(patient.Id).Then(() =>
        {
            this.loadscreen.gameObject.SetActive(false);
        });
    }

    private void OpenProcedure(Patient patient)
    {
        this.loadscreen.gameObject.SetActive(true);
        ApiService.GetStartedProcedure(patient.Id.ToString()).Then((procedure) =>
        {
            this.loadscreen.gameObject.SetActive(false);
            this.cubesView.Setup(procedure.Id);
            this.OpenNewScreen(this.cubesView.gameObject);
        });
    }

    private void OpenProcedureResult(Patient patient)
    {
        this.loadscreen.gameObject.SetActive(true);
        ApiService.GetStartedProcedure(patient.Id.ToString()).Then(procedure =>
        {
            ApiService.GetProcedureScreen(procedure.Id.ToString()).Then(bytes =>
            {
                Debug.Log(bytes.Length);
                this.OpenNewScreen(this.resultView.gameObject);
                this.resultView.ShowResult(procedure, bytes);
            }).Catch(ex => Debug.Log(ex)).Finally(() => this.loadscreen.gameObject.SetActive(false));
        });
    }

    private void BackToPreviousScreen()
    {
        if (openedScreens.Count > 1)
        {
            this.openedScreens.Pop()?.SetActive(false);
            this.openedScreens.Peek()?.SetActive(true);
        }
        this.loadscreen.SetActive(false);
    }

    private void OpenNewScreen(GameObject screen, bool addToScreensStack = true)
    {
        this.openedScreens.Peek()?.SetActive(false);
        screen.SetActive(true);
        this.openedScreens.Push(screen);
    }

    private void ShowLoadingScreen(bool loadingIsActive)
    {
        this.loadscreen.SetActive(loadingIsActive);
    }

    private bool ValidateLoginModel(LoginModel model)
    {
        if (ValidationHelper.IsEmailValid(model.Username))
        {
            if (ValidationHelper.IsPasswordValid(model.Password))
            {
                return true;
            }
            else
            {
                this.authView.ShowMessage("Password isn`t valid");
            }
        }
        else
        {
            this.authView.ShowMessage("Email isn`t valid");
        }

        return false;
    }

    private bool ValidateSigninModel(SigninModel model, string confirmedPassword)
    {
        if (this.ValidateLoginModel(model))
        {
            if (ValidationHelper.DoPasswordsMatch(model.Password, confirmedPassword))
            {
                return true;
            }
            else
            {
                this.authView.ShowMessage("Password don`t match");
            }
        }

        return false;
    }
}
