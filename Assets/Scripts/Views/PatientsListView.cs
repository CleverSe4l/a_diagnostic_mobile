﻿using SomeGameAPI.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PatientsListView : MonoBehaviour
{
    [SerializeField]
    private Transform patientRowsParent;

    [SerializeField]
    private PatientRow patientRowPrefab;

    public void Setup(List<Patient> patients, UnityAction<Patient> onStartClick, UnityAction<Patient> onStopClick = null, UnityAction<Patient> onOpenClick = null)
    {
        foreach (var row in this.patientRowsParent.GetComponentsInChildren<PatientRow>())
        {
            Destroy(row.gameObject);
        }
        patients.ForEach(x => Instantiate(patientRowPrefab, patientRowsParent).Setup(x, onStartClick, onStopClick, onOpenClick));
    }
}
