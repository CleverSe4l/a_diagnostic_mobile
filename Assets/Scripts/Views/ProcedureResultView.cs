﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

class ProcedureResultView : MonoBehaviour
{
    [SerializeField]
    private RawImage screenshot;

    [SerializeField]
    private Text resultLabel;

    public void ShowResult(Procedure procedure, byte[] bytes)
    {
        this.resultLabel.text = $"{LocalizationManager.CurrentSet.Heartrate}: {procedure.Heartrate} &\n{LocalizationManager.CurrentSet.Temperature}: {procedure.Temperature}";
        var texture = new Texture2D(0, 0, TextureFormat.RGB24, false);
        texture.LoadImage(bytes);
        screenshot.texture = texture;
    }
}