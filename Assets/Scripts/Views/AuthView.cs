﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AuthView : MonoBehaviour
{
    [SerializeField]
    private InputField email, password, confirmPassword, firstName, lastName;

    [SerializeField]
    private Text errorLabel;

    [SerializeField]
    private Button loginButton, signinButton;

    [SerializeField]
    private Toggle signInToggle;

    public void ShowMessage(string message)
    {
        this.errorLabel.text = message;
        this.errorLabel.enabled = true;
    }

    public void StopShowingMessage()
    {
        this.errorLabel.enabled = false;
    }

    public void Setup(UnityAction<string, string> Login, UnityAction<string, string, string> Signin)
    {
        this.loginButton.onClick.AddListener(() => Login(this.email.text, this.password.text));
        this.signinButton.onClick.AddListener(() => Signin(this.email.text, this.password.text, this.confirmPassword.text));
        this.signInToggle.onValueChanged.AddListener(this.SwitchMode);
    }

    private void SwitchMode(bool isSignIn)
    {
        this.loginButton.gameObject.SetActive(!isSignIn);
        this.signinButton.gameObject.SetActive(isSignIn);
        this.confirmPassword.gameObject.SetActive(isSignIn);
        this.firstName.gameObject.SetActive(isSignIn);
        this.lastName.gameObject.SetActive(isSignIn);
    }
}