﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PatientRow : MonoBehaviour
{
    [SerializeField]
    private Text nameLabel;

    [SerializeField]
    private Button startButton, stopButton, openButton;

    public Text StartLabel, OpenLabel;

    public void Setup(Patient patient, UnityAction<Patient> onStart, UnityAction<Patient> onStop = null, UnityAction<Patient> onOpen = null)
    {
        this.nameLabel.text = $"{patient.FirstName}_{patient.LastName}";
        this.startButton.onClick.AddListener(() => onStart(patient));
        this.openButton.gameObject.SetActive(onOpen != null);
        this.openButton.onClick.AddListener(() => onOpen(patient));
        this.stopButton.gameObject.SetActive(onStop != null);
        this.stopButton.onClick.AddListener(() => onStop(patient));
    }
}
