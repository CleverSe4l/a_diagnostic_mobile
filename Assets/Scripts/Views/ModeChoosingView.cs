﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ModeChoosingView : MonoBehaviour
{
    [SerializeField]
    private Button procedureManagmentMode, patientMode;

    public void Setup(UnityAction onProceduresManagmentModeChosen, UnityAction onPatientModeChosen)
    {
        this.procedureManagmentMode.onClick.AddListener(onProceduresManagmentModeChosen);
        this.patientMode.onClick.AddListener(onPatientModeChosen);
    }
}
