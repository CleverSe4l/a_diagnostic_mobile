﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class CubesView : MonoBehaviour
{
    [SerializeField]
    private List<Button> cubeSpawnButtons;

    [SerializeField]
    private Transform spawnTransform;

    [SerializeField]
    private Image cubePrefab;

    private int procedureId;
    
    private Vector3 SpawnPosition => this.spawnTransform.position;

    public void Setup(int procedureId)
    {
        this.procedureId = procedureId;
    }

    private void OnEnable()
    {
        SendScreenshot();
    }

    private void Start()
    {
        this.cubeSpawnButtons.ForEach(x => x.onClick.AddListener(() => this.SpawnCube(x.GetComponent<Graphic>().color)));
    }

    private void SendScreenshot()
    {
        var screen = ScreenCapture.CaptureScreenshotAsTexture();
        ApiService.postProceduresScreen(this.procedureId.ToString(), screen.EncodeToJPG()).Then(() =>
        {
            Debug.Log("screen sended");
            if (this.gameObject.activeInHierarchy) Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(x => this.SendScreenshot());
        });
    }

    private void SpawnCube(Color color)
    {
        Debug.Log(color);
        var cube = Instantiate(this.cubePrefab, this.transform);
        cube.transform.position = this.SpawnPosition;
        cube.color = color;
    }
}
