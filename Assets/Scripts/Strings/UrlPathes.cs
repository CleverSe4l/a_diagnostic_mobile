﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

class UrlPathes : MonoBehaviour
{
    public static string Url { get; set; } = "http://localhost:4000/api/";

    public static string Users { get; set; } = Url + "Users/";

    public static string Login { get; set; } = Users + "Login/";

    public static string Signin { get; set; } = Users + "Signin/";

    public static string Patients { get; set; } = Url + "Patients/";

    public static string Procedures { get; set; } = Url + "Procedures/";

    public static string StartedProcedure { get; set; } = Procedures + "started/";

    public static string ProcedureScreenshot { get; set; } = Procedures + "photo/";

    public static string ProcedureHeartrate { get; set; } = Procedures + "heart/";

    public static string ProcedureTemperature { get; set; } = Procedures + "temperature/";

    public static string StartProcedure { get; set; } = Procedures + "Start/";

    public static string StopProcedure { get; set; } = Procedures + "Stop/";

    [SerializeField]
    private InputField urlField;

    public void SetUrl(string url)
    {
        Url = url;
        
        Users = Url + "Users/";

        Login = Users + "Login/";

        Signin = Users + "Signin/";

        Patients = Url + "Patients/";

        Procedures = Url + "Procedures/";

        StartedProcedure = Procedures + "started/";

        ProcedureScreenshot = Procedures + "photo/";

        ProcedureHeartrate = Procedures + "heart/";

        ProcedureTemperature = Procedures + "temperature/";

        StartProcedure = Procedures + "Start/";
    }

    private void Awake()
    {
        this.urlField.text = Url;
        this.urlField.onValueChanged.AddListener(this.SetUrl);
    }
}