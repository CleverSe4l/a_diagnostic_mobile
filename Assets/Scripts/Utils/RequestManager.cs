﻿using System;
using System.Collections.Generic;
using System.Text;
using RSG;
using UniRx;
using UnityEngine;

class RequestManager
{
    private static string authToken;

    private static string BearerAuthToken
    {
        get
        {
            return "Bearer " + authToken;
        }
    }

    public static IPromise Get(string url)
    {
        Promise promise = new Promise();
        Dictionary<string, string> headers = new Dictionary<string, string>
        {
            { "Authorization", BearerAuthToken }
        };
        var www = ObservableWWW.GetWWW(url, headers);
        www.Subscribe(
            x => promise.Resolve(),
            ex => promise.Reject(ex));

        return promise;
    }

    public static IPromise<T> Get<T>(string url)
    {
        Promise<T> promise = new Promise<T>();
        Dictionary<string, string> headers = new Dictionary<string, string>
        {
            { "Authorization", BearerAuthToken }
        };
        var www = ObservableWWW.GetWWW(url, headers);
        www.Subscribe(
            x =>
            {
                promise.Resolve(DataParser.DeserializeObject<T>(x.text));
            },
            ex => promise.Reject(ex));

        return promise;
    }

    public static IPromise Post(string url, object postData)
    {
        Promise promise = new Promise();
        var www = GeneratePostRequest(url, postData);
        www.Subscribe(
            x =>
            {
                promise.Resolve();
            },
            ex => promise.Reject(ex));

        return promise;
    }

    public static IPromise<T> Post<T>(string url, object postData)
    {
        Promise<T> promise = new Promise<T>();
        var www = GeneratePostRequest(url, postData);
        www.Subscribe(
            x =>
            {
                promise.Resolve(DataParser.DeserializeObject<T>(x.text));
            },
            ex => promise.Reject(ex));

        return promise;
    }

    public static IPromise PostTexture(string url, string filename, byte[] data)
    {
        Promise promise = new Promise();
        Dictionary<string, string> headers = new Dictionary<string, string>
        {
            { "Authorization", BearerAuthToken }
        };

        WWWForm form = new WWWForm();
        form.AddBinaryData(filename, data, filename);
        var www = ObservableWWW.PostWWW(url, form, headers);
        www.Subscribe(
            x =>
            {
                promise.Resolve();
            },
            ex =>
            {
                Debug.LogError(ex);
                promise.Reject(ex);
            });

        return promise;
    }

    public static IPromise<byte[]> GetTexture(string url)
    {
        Promise<byte[]> promise = new Promise<byte[]>();
        Dictionary<string, string> headers = new Dictionary<string, string>
        {
            { "Authorization", BearerAuthToken }
        };
        var www = ObservableWWW.GetWWW(url, headers);
        www.Subscribe(
            x =>
            {
                promise.Resolve(x.bytes);
            },
            ex => promise.Reject(ex));

        return promise;
    }

    public static Promise Auth(string url, LoginModel model)
    {
        Promise promise = new Promise();
        string jsonData = DataParser.SerializeObject(model);
        Dictionary<string, string> headers = new Dictionary<string, string>
        {
            { "Content-Type", "application/json" }
        };
        byte[] bytes = Encoding.UTF8.GetBytes(jsonData);
        var www = ObservableWWW.PostWWW(url, bytes, headers);
        www.Subscribe(
            x =>
            {
                var user = DataParser.DeserializeObject<User>(x.text);
                if (user.Token != string.Empty) authToken = user.Token.Replace("\"", string.Empty);
                Debug.Log(authToken);
                promise.Resolve();
            },
            ex => promise.Reject(ex));

        return promise;
    }
    
    private static IObservable<WWW> GeneratePostRequest(string url, object postData)
    {
        string jsonData = DataParser.SerializeObject(postData);
        Dictionary<string, string> headers = new Dictionary<string, string>
        {
            { "Authorization", BearerAuthToken },
            { "Content-Type", "application/json" }
        };
        byte[] bytes = Encoding.UTF8.GetBytes(jsonData);
        return ObservableWWW.PostWWW(url, bytes, headers);
    }
}