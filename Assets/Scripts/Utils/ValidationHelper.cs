﻿using System.Text.RegularExpressions;

public static class ValidationHelper
{
    private const int PasswordMinLenght = 6;

    public static bool IsEmailValid(string text)
    {
        if (text.Length == 0)
        {
            return false;
        }
        else
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(text);
            if (!match.Success)
            {
                return false;
            }
        }

        return true;
    }

    public static bool IsPasswordValid(string text)
    {
        if (text.Length == 0)
        {
            return false;
        }
        else if (text.Length < PasswordMinLenght)
        {
            return false;
        }

        return true;
    }

    public static bool DoPasswordsMatch(string password, string confPassword)
    {
        if (password != confPassword)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
