﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

public static class DataParser
{
    public static T DeserializeObject<T>(string data)
    {
        return JsonConvert.DeserializeObject<T>(data);
    }

    public static string SerializeObject(object obj)
    {
        return JsonConvert.SerializeObject(obj);
    }
}
