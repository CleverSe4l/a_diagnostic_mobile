﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizationManager : MonoBehaviour
{
    [SerializeField]
    private Text Login, Password, Confirm, Firstname, Secondname, LoginSwitchText, SigninSwitchText, LoginButtonText, SigninButtonText, ChooseMode, ProceduresManaging, PatientMode;

    [SerializeField]
    private PatientRow patientRowPrefab;

    [SerializeField]
    private Toggle EnToggle;

    public static StringsSet CurrentSet { get; set; }

    private void Awake()
    {
        this.EnToggle.onValueChanged.AddListener(value => this.ChangeLanguage(value ? StringsSet.EnglishSet : StringsSet.UkrainianSet));
        this.ChangeLanguage(StringsSet.EnglishSet);
    }

    private void ChangeLanguage(StringsSet stringsSet)
    {
        this.Login.text = stringsSet.Login;
        this.Password.text = stringsSet.Password;
        this.Confirm.text = stringsSet.Confirm;
        this.Firstname.text = stringsSet.Firstname;
        this.Secondname.text = stringsSet.Secondname;
        this.LoginSwitchText.text = stringsSet.LoginButtonText;
        this.SigninSwitchText.text = stringsSet.SigninButtonText;
        this.LoginButtonText.text = stringsSet.LoginButtonText;
        this.SigninButtonText.text = stringsSet.SigninButtonText;
        this.ChooseMode.text = stringsSet.ChoseMode;
        this.ProceduresManaging.text = stringsSet.ProceduresManagingButton;
        this.PatientMode.text = stringsSet.PatientModeButton;
        this.patientRowPrefab.StartLabel.text = stringsSet.StartButton;
        this.patientRowPrefab.OpenLabel.text = stringsSet.OpenButton;
        CurrentSet = stringsSet;
    }
}
